# MagicBuilder
Magic Builder是一款语音算法在线配置工具，可以配置语音前处理算法参数，可以自定义唤醒词和语音TTS回复等配置.
# 下载方法
进入 https://gitlab.com/nationalchip/magicbuilder/-/releases, 根据系统下载对应的最新版本.
# 安装方法
## Linux
1. chomd +x MagicBuilder-for-linux.AppImage
2. 双击运行 MagicBuilder-for-linux.AppImage.
## Windows
1. 支持 win7 sp1, win 8, win 10.
2. 解压缩 CH341SER.rar, 运行 SETUP.EXE 安装 usb2uart 驱动.
3. 解压缩 MagicBuilder-for-windows.rar, 运行 MagicBuilder.exe
## Mac
1. 解压缩 MagicBuilder-for-mac.zip
2. 解压缩 CH341SER_MAC.ZIP, 阅读 CH341SER_MAC 下的 ReadMe.pdf 安装 usb2uart 驱动.
3. 双击 MagicBuilder.app


